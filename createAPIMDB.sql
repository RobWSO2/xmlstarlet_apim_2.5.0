#create the databases

drop database WSO2AM_DB;
drop database WSO2MB_DB;
drop database WSO2METRICS_DB;

create database if not exists WSO2AM_DB character set latin1;
use WSO2AM_DB;
source /opt/wso2/Desktop/wso2am-2.2.0/dbscripts/apimgt/mysql.sql;
grant all on WSO2AM_DB.* TO apimuser@localhost identified by "apimpass";
flush privileges;
 
create database if not exists WSO2MB_DB character set latin1;
use WSO2MB_DB;
source /opt/wso2/Desktop/wso2am-2.2.0/dbscripts/mb-store/mysql-mb.sql;
grant all on WSO2MB_DB.* TO apimuser@localhost identified by "apimpass";
flush privileges;

create database if not exists WSO2METRICS_DB character set latin1;
use WSO2METRICS_DB;
source /opt/wso2/Desktop/wso2am-2.2.0/dbscripts/metrics/mysql.sql;
grant all on WSO2METRICS_DB.* TO apimuser@localhost identified by "apimpass";
flush privileges;
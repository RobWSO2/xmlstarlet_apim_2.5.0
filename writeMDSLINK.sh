#!/bin/sh



function insertDB () {

xmlstarlet ed --append //datasources/datasource[last\(\)] --type elem -n mydatasource $XMLINPUT | \
xmlstarlet ed --subnode //mydatasource --type elem -n name -v $V_NAME | \
xmlstarlet ed --subnode //mydatasource --type elem -n jndiConfig | \
xmlstarlet ed --subnode //mydatasource/jndiConfig --type elem -n name -v $V_JNDICONFIG | \
#xmlstarlet ed --subnode //mydatasource/jndiConfig --type elem -n properties | \
#xmlstarlet ed --subnode //mydatasource/jndiConfig/properties --type elem -n myproperty | \
#xmlstarlet ed --insert //mydatasource/jndiConfig/properties/myproperty --type attr -n name -v "$V_PROPERTY" | \
#xmlstarlet ed --rename //mydatasource/jndiConfig/properties/myproperty -v property | \
xmlstarlet ed --subnode //mydatasource --type elem -n mydefinition | \
xmlstarlet ed --insert //mydatasource/mydefinition --type attr -n type -v $V_DEFINITION | \
xmlstarlet ed --subnode //mydatasource/mydefinition --type elem -n configuration | \
xmlstarlet ed --subnode //mydatasource/mydefinition/configuration --type elem -n url -v $V_URL | \
xmlstarlet ed --subnode //mydatasource/mydefinition/configuration --type elem -n username -v $V_USERNAME | \
xmlstarlet ed --subnode //mydatasource/mydefinition/configuration --type elem -n password -v $V_PASSWORD | \
xmlstarlet ed --subnode //mydatasource/mydefinition/configuration --type elem -n driverClassName -v $V_DRIVER | \
xmlstarlet ed --subnode //mydatasource/mydefinition/configuration --type elem -n maxActive -v $V_MAXACTIVE | \
xmlstarlet ed --subnode //mydatasource/mydefinition/configuration --type elem -n maxWait -v $V_MAXWAIT | \
xmlstarlet ed --subnode //mydatasource/mydefinition/configuration --type elem -n testOnBorrow -v $V_TESTONBORROW | \
xmlstarlet ed --subnode //mydatasource/mydefinition/configuration --type elem -n validationQuery -v "$V_VALIDATIONQUERY" | \
xmlstarlet ed --subnode //mydatasource/mydefinition/configuration --type elem -n validationInterval -v $V_VALIDATIONINTERVAL | \
xmlstarlet ed --subnode //mydatasource/mydefinition/configuration --type elem -n defaultAutoCommit -v $V_DEFAULTAUTOCOMMIT | \
xmlstarlet ed --rename //mydatasource/mydefinition -v definition | \
xmlstarlet ed --rename //mydatasource -v datasource >$XMLOUTPUT
}





echo step 1
echo changing masterdatasource

cd /opt/wso2/Desktop/ENVTWO/wso2am-2.2.0/repository/conf/datasources

XMLFILEORG=master-datasources.xml
XMLINPUT=$XMLFILEORG
XMLOUTPUT=$XMLFILEORG".man"
XMLCOPY=$XMLFILEORG".old"
cp $XMLFILEORG $XMLCOPY
 
xmlstarlet ed --inplace -P -d //datasource[name=\"WSO2AM_DB\"] $XMLINPUT
#/opt/wso2/Desktop/ENV/wso2am-2.2.0/repository/conf/datasources/master-datasources.xml


#-v jdbc/WSO2AM_DB

V_NAME=WSO2AM_DB
V_JNDICONFIG=jdbc/WSO2AM_DB
V_DEFINITION="RDBMS"
V_CONFIGURATION=""
V_URL="jdbc:mysql://localhost:3306/WSO2AM_DB"
V_PROPERTY=
V_USERNAME=apimuser
V_PASSWORD=apimpass
V_DRIVER=com.mysql.jdbc.Driver
V_MAXACTIVE=50
V_MAXWAIT=60000
V_TESTONBORROW=true
V_VALIDATIONQUERY="SELECT 1"
V_VALIDATIONINTERVAL=30000
V_DEFAULTAUTOCOMMIT=false

echo step 2
insertDB

echo dit zijn ze 

XMLINPUT=$XMLOUTPUT
XMLOUTPUT=$XMLFILEORG
xmlstarlet ed --inplace -P -d //datasource[name=\"WSO2_MB_STORE_DB\"] $XMLINPUT

echo $XMLINPUT
echo $XMLOUTPUT

V_NAME=WSO2_MB_STORE_DB
V_JNDICONFIG=WSO2MBStoreDB
V_DEFINITION="RDBMS"
V_CONFIGURATION=""
V_URL="jdbc:mysql://localhost:3306/WSO2MB_DB"
V_PROPERTY=
V_USERNAME=apimuser
V_PASSWORD=apimpass
V_DRIVER=com.mysql.jdbc.Driver
V_MAXACTIVE=50
V_MAXWAIT=60000
V_TESTONBORROW=true
V_VALIDATIONQUERY="SELECT 1"
V_VALIDATIONINTERVAL=30000
V_DEFAULTAUTOCOMMIT=false


insertDB


echo step 3

XMLFILEORG=metrics-datasources.xml
XMLINPUT=$XMLFILEORG
XMLOUTPUT=$XMLFILEORG".man"
XMLCOPY=$XMLFILEORG".old"
cp $XMLFILEORG $XMLCOPY
 


V_NAME=WSO2_METRICS_DB2
V_JNDICONFIG=jdbc/WSO2MetricsDB
V_DEFINITION="RDBMS"
V_CONFIGURATION=""
V_URL="jdbc:mysql://localhost:3306/WSO2METRICS_DB"
V_PROPERTY=
V_USERNAME=apimuser
V_PASSWORD=apimpass
V_DRIVER=com.mysql.jdbc.Driver
V_MAXACTIVE=50
V_MAXWAIT=60000
V_TESTONBORROW=true
V_VALIDATIONQUERY="SELECT 1"
V_VALIDATIONINTERVAL=30000
V_DEFAULTAUTOCOMMIT=false

insertDB

cp  $XMLOUTPUT $XMLFILEORG
xmlstarlet ed --inplace -P -d //datasource[name=\"WSO2_METRICS_DB\"] $XMLINPUT
xmlstarlet ed --inplace -u //datasource[name=\"WSO2_METRICS_DB2\"]/name -v WSO2_METRICS_DB $XMLINPUT
#xmlstarlet ed --inplace -P -d //datasource[name=\"WSO2_METRICS_DB\"] $XMLINPUT

echo done